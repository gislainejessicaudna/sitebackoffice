import { Auth } from 'aws-amplify';

import { masks } from './maskService';

const SignInService = async (username, password) => {
  try {
    const res = await Auth.signIn(username, password);
    return [null, res];
  } catch (error) {
    return [error, null];
  }
};

const SignOutService = async () => {
  try {
    const res = await Auth.signOut();
    return [null, res];
  } catch (error) {
    return [error, null];
  }
};

const mapUserService = (userData) => {
  const data = {
    cpf: masks.cpf(userData.cpf),
    name: userData.name,
    email: userData.email,
    cellphone: masks.cellphone(userData.cellphone),
    restoreId: userData.restoreId,
  };

  if (userData.address) {
    data.address = {
      zipCode: masks.zipCode(userData.address?.zipCode),
      street: userData.address?.street,
      number: userData.address?.number,
      complement: userData.address?.complement,
      neighborhood: userData.address?.neighborhood,
      city: userData.address?.city,
      stateInitials: userData.address?.stateInitials,
      stateName: userData.address?.stateName,
    };
  } else {
    data.address = null;
  }

  return data;
};


export {
  SignInService,
  SignOutService,
  mapUserService,
};
