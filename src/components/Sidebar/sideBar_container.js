import React, { useState } from 'react';
import { useRouteMatch, useLocation } from 'react-router-dom';
import { match } from 'react-router-dom';
import SideBarView from './sideBar__view';

const SideBarContainer = () => {
  const [active, setActive] = useState(true)
  const location = useLocation()

  let match = useRouteMatch({
    path: location.pathname,
    exact: true
  });

  const active = false  ||  match ? "active" : ""
  console.tron.log("side")
  console.tron.log(match.path)

  function handleClick(){
    setActive(!active)
  }
  return (
    <SideBarView
      active={active}
      handleClick={handleClick}
      match={match.path}
    />
  );
}

export default SideBarContainer;
