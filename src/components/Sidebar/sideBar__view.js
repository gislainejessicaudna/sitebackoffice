import React from 'react';
import { MdPerson, MdDashboard, MdSettings, MdShowChart } from 'react-icons/md';
import { NavLink } from 'react-router-dom'

import logo from '../../assets/svg/uDNA_logo_icone_branco.svg';

import './styles.scss'

const SideBarView = ({ match }) => (
  <div className="Container-sidebar">
    <div className="Logo">
      <NavLink to="/">
        <img className="Link"
          src={logo}
          alt="logo"
        />
      </NavLink>
      <h1> Udna </h1>
    </div>

    <div className="list">
      <NavLink to = "/dashboard" >
        < div className="Navegation" active={ match === "/dashboard" ? true : false }>
          <MdDashboard size={24} color="#fff" />
          <h2> Dashboard </h2>
        </div>
      </NavLink>

      <NavLink to = "/usuarios" >
      < div className="Navegation">
        <MdPerson size={24} color="#fff" />
        <h2> Usuários </h2>
      </div>
      </NavLink>

      <NavLink to = "/relatorios" >
      < div className="Navegation">
        <MdShowChart size={24} color="#fff" />
        <h2> Relatórios </h2>
      </div>
      </NavLink>

      <NavLink to = "/configuracao" >
      < div className="Navegation">
        <MdSettings size={24} color="#fff" />
        <h2> Configuração </h2>
      </div>
      </NavLink>
    </div>
  </div>
);

export default SideBarView;

