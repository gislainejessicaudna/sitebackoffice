import propTypes from 'prop-types';
import React from 'react';

import './styles.scss';

const SearchBarView = ({ userName,  title, handleSignout }) => (
  <div className="Container-Search">
    <h1> {title} </h1>
    <div className="buscar">
      <span> 
        {userName ? `Admin: ${userName}`: "Administrador "}
      </span>
      
      <button className="search" onClick={handleSignout}>
        Sair
      </button>
    </div>
  </div>
);

SearchBarView.propTypes = {
  title: propTypes.string,
  handleSignout: propTypes.func.isRequired,
  userName: propTypes.string,
};

SearchBarView.defaultProps = {
  title: '',
};

export default SearchBarView;
