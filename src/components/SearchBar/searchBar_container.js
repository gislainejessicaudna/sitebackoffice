import React from 'react';
import { useHistory } from 'react-router-dom';

import { useUser } from '../../providers/UserProvider';
import { SignOutService } from '../../services/userService';

import SearchBarView from './searchBar__view';

const SearchBarContainer = () => {
  const history = useHistory()
  const { user, setUser } = useUser()

  const  handleSignout = async () => {
    setUser({ loading: true })
    await SignOutService()
    setUser({ cpf: "", loading: false, session:"" , signed: false })

    alert("Sessão atual será encerrada")
    history.push('/')
  }

  return (
    <SearchBarView
      handleSignout={handleSignout}
      userName={ user.cpf }
    />
  );
};

export default SearchBarContainer;
