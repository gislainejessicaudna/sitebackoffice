import propTypes from 'prop-types';
import React from 'react';
import { FaSpinner } from 'react-icons/fa';

import './styles.scss'

const ButtonView = ({
  children,
  type,
  disabled,
  buttonIcon,
  onClick,
  loading,
}) => (
  <button
    className="mybutton"
    type={type}
    disabled={disabled}
    onClick={onClick}
    loading={loading}
  >
  { loading ?
  (
    <FaSpinner size={24} color="#fff" />
  ) : (
    buttonIcon && (
      <buttonIcon size={24} color="#fff" />
    )
  )}
    <span>
      {` ${children} ` || 'Button'}
    </span>
  </button>
);

ButtonView.propTypes = {
  children: propTypes.node.isRequired,
  type: propTypes.string,
  disabled: propTypes.bool,
  buttonIcon: propTypes.node,
  onClick: propTypes.func.isRequired,
};

ButtonView.defaultProps = {
  type: '',
  disabled: false,
  buttonIcon: null,
};

export default ButtonView;
