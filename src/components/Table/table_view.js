import React from 'react';

import './styles.scss'

export default function Table({ dados }) {
  return (
    <table className="Container-table">
       <thead>
          <tr>
            <th> ID </th>
            <th> Nome </th>
            <th> CPF </th>
            <th> Email </th>
            <th> Telefone </th>
          </tr>
        </thead>

        <tbody>
          { dados && dados.map( dado =>
              <tr>
                <td>{dado.id}</td>
                <td>{dado.name}</td>
                <td>{dado.cpf}</td>
                <td>{dado.email}</td>
                <td>{dado.cellphone}</td>
              </tr>
            )
          }
        </tbody>
    </table>
  );
}
