import propTypes from 'prop-types';
import React from 'react';

import './styles.scss';

const InputView = ({
  name,
  type,
  placeholder,
  maxLength,
  pattern,
  onChange,
  inputIcon,
}) => (
  <div className="Container-input" >
    {inputIcon}
    <input 
      className="myInput"
      name={name}
      type={type}
      placeholder={placeholder}
      maxLength={maxLength}
      pattern={pattern}
      onChange={onChange}
    />
  </div>
);

InputView.propTypes = {
  name: propTypes.string,
  type: propTypes.string,
  placeholder: propTypes.string,
  maxLength: propTypes.string,
  pattern: propTypes.string,
  onChange: propTypes.func.isRequired,
  inputIcon: propTypes.node,
};

InputView.defaultProps = {
  name: '',
  type: '',
  placeholder: '',
  maxLength: '100',
  pattern: '',
  inputIcon: null,
};

export default InputView;
