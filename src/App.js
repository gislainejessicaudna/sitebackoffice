import { ApolloProvider } from '@apollo/react-hooks';
import React from 'react';

import { UserProvider } from './providers/UserProvider';
import AppRoutes from './routes';
import { udnaAPI } from './services/udnaAPIService';

export default function uDNAAPP() {
  return (
      <ApolloProvider client={udnaAPI}>
        <UserProvider>
          <AppRoutes />
        </UserProvider>
      </ApolloProvider>
  );
}
