import { useFormik } from 'formik';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import { useUser } from '../../providers/UserProvider';
import { SignInService } from '../../services/userService';
import { validationSchemaAuth } from '../../services/validationService/validationAuthService';

import AuthView from './auth__view';

const AuthContainer = () => {
  const history = useHistory();
  const { setUser, user } = useUser();
  const [initialValues] = useState({
    cpf: '',
    password: '',
  });

  const {
    values,
    errors,
    touched,
    // setFieldValue,
    handleChange,
    handleBlur,
    submitForm,
  } = useFormik({
    initialValues,
    validationSchema: validationSchemaAuth,
    onSubmit,
    validateOnChange: true,
    validateOnMount: true,
  });

  const SignIn = async (username, password) => {
    setUser({ cpf: "", loading: true, session: "wait" });
    const [error, _] = await SignInService(username, password);
    if (error) {
      console.tron.log(error);
      setUser({ cpf: "", loading: false, session: "fail" });
      return error;
    }
    setUser({ cpf: username, loading: false, session: "on" });
    history.push('/dashboard');
  };

  async function onSubmit(data) {
    console.tron.log(data);
    SignIn(data.cpf, data.password);
  }
  return (
    <AuthView
      values={values}
      errors={errors}
      touched={touched}
      handleChange={handleChange}
      handleBlur={handleBlur}
      submitForm={submitForm}
      loading={user.loading}
      session={user.session}
    />
  );
};

export default AuthContainer;
