import propTypes from 'prop-types';
import React from 'react';
import { MdEmail as CPFIcon, MdLock as PasswordIcon } from 'react-icons/md';

import logo from '../../assets/images/uDNA_logo.png';

import { Form } from '@rocketseat/unform';

import AuthButton from '../../components/Button';
import AuthInput from '../../components/Input';

import './styles.scss';

const AuthView = ({
  values,
  errors,
  touched,
  handleChange,
  handleBlur,
  submitForm,
  loading,
  session,
}) => (
  <div className="Container-auth">
    <div className="Formulario">
    <Form
      onSubmit={submitForm}
      style={{ 
        justifyContent: 'center', 
        alignItems: 'center', 
        display: 'flex', 
        flexDirection: 'column'
      }}
    >
      <img className="Logo-auth" src={logo} alt="logo" />
      <p> { session === "fail" ? "Falha na autenticação, tente novamente!!!" : ""} </p>
      <div className="InputContainer">
        <span> CPF </span>
        <AuthInput
          value={values.cpf}
          name="cpf"
          placeholder="CPF"
          maxLength="11"
          inputIcon={(
            <CPFIcon
              size={20}
              style={{ margin: 4 }}
              color={'#554086'}
            />
)}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {errors.cpf && touched.cpf
        && <text className="TextError">{errors.cpf}</text>}
      </div>
      <div className="InputContainer">
        <span> Senha </span>
        <AuthInput
          value={values.password}
          name="password"
          type="password"
          placeholder="Senha"
          inputIcon={(
            <PasswordIcon
              size={20}
              style={{ margin: 4 }}
              color={"#554086"}
            />
)}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </div>
      <AuthButton
        type="submit"
        disabled={Object.keys(errors).some(Boolean)}
        onClick={submitForm}
        loading={loading}    
      >
        Entrar
      </AuthButton>
    </Form>
    </div>
  </div>
);

AuthView.propTypes = {
  values: propTypes.shape({
    cpf: propTypes.string,
    password: propTypes.string,
  }),
  errors: propTypes.shape({
    cpf: propTypes.string,
    password: propTypes.string,
  }),
  touched: propTypes.shape({
    cpf: propTypes.bool,
    password: propTypes.bool,
  }),
  handleChange: propTypes.func.isRequired,
  handleBlur: propTypes.func.isRequired,
  submitForm: propTypes.func.isRequired,
  loading: propTypes.bool,
  session: propTypes.string,
};

AuthView.defaultProps = {
  values: {
    cpf: '',
    password: '',
  },
  errors: {
    cpf: '',
    password: '',
  },
  touched: {
    cpf: false,
    password: false,
  },
  loading: false,
  session: "",
};

export default AuthView;
