import { useQuery } from '@apollo/react-hooks';
import React, { useEffect,  useState } from 'react';

import { getUserCustomized, listUsersCustomized , listKitsCustomized} from '../../graphql/queriesCustomized';
import { useUser } from '../../providers/UserProvider';

import HomeView from './home__view';

const HomeContainer = () => {
  const { user } = useUser();
  const [users, setUsers] = useState([])

  const { data: userData } = useQuery(getUserCustomized, {
    variables: { id: user.cpf },
  });

  const { data: listUsersData } = useQuery(listUsersCustomized);
  const { data: kits } = useQuery(listKitsCustomized);
  
  console.tron.log(kits)

  useEffect(() => {
    if (listUsersData) {
      console.tron.log(" Lista de usuários ", listUsersData.listUsers.items)
      setUsers(listUsersData.listUsers.items)
    }
  }, [listUsersData])

  useEffect(() => {
    if (userData) {
      console.tron.log("Dados query", userData);
    }
  }, [userData]);

  return (
    <HomeView
      users={users}
    />
  );
};

export default HomeContainer;
