import propTypes from 'prop-types';
import React from 'react';

import SearchBar from '../../components/SearchBar';
import SideBar from '../../components/Sidebar';
import Table from '../../components/Table';

import './styles.scss';

const HomeView = ({ users }) => (
  <div className="Container-home">
      <SideBar />
      <div className="DashBoard-home">
        <SearchBar />
        < div className="BoxTable-home">
          <div className="TitleBox-home">
            <h1> Usuários </h1>
          </div>
          <Table dados={users} />
        </div>
      </div>
      </div>
);

HomeView.propTypes = {
  users: propTypes.shape({
  })
}

export default HomeView;

/**
 * 
  <div className="Container-home">
    <SideBar />
    <div className="DashBoard-home">
      <SearchBar />
      < div className="BoxTable">
        <div className="TitleBox">
          <h1> Usuários </h1>
        </div>
        <Table dados={users} />
      </div>
    </div>
  </div>
 */