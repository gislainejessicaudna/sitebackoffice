import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
} from 'react-router-dom';

import Route from './Route';

import Auth from '../pages/Auth';
import Home from '../pages/Home';

import '../styles/global.scss';

const App = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={Auth} />
      <Route path="/dashboard" component={Home} isPrivate/>
      <Route path="/usuarios" component={Home} isPrivate/>
      <Route path="/relatorios" component={Home} isPrivate />
      <Route path="/configuracao" component={Home} isPrivate />
    </Switch>
  </Router>
);

export default App;
