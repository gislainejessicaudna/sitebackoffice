import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Auth } from 'aws-amplify';

import { useUser } from '../providers/UserProvider';

import AuthLayout from '../layouts/AuthContainer';
import DefaultLayout from '../layouts/DefaultContainer';

export default function RouteWrapper({
  component: Component,
  isPrivate,
  ...rest
}) {
  const { user } = useUser()
  const [signed, setSigned] = useState(false)

  const autenticar = async () => {
    const { accessToken } = await Auth.currentSession();

    if (accessToken) {
      const { payload } = accessToken
      if(user){
        if (payload.username === user.cpf){
          setSigned(true)
        } else {
          setSigned(false)
        }
      }
    }
  }

  autenticar()

  if (!signed && isPrivate) {
    return <Redirect to="/" />;
  }

  if (signed && !isPrivate) {
    return <Redirect to="/dashboard" />;
  }

  const Layout = signed ? DefaultLayout : AuthLayout;
  // const Layout = DefaultLayout
  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired,
};

RouteWrapper.defaultProps = {
  isPrivate: false,
};
