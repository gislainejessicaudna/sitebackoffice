import Reactotron from 'reactotron-react-js';

console.tron = Reactotron;

Reactotron
  .configure()
  .connect();
