/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getExams = /* GraphQL */ `
  query GetExams($username: String) {
    getExams(username: $username) {
      id
      name
      price
      payment
      installments
      status
      date
    }
  }
`;
export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      name
      email
      cellphone
      cpf
      restoreId
      address {
        id
        zipCode
        street
        number
        complement
        neighborhood
        city
        stateInitials
        stateName
        owner
      }
      owner
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        email
        cellphone
        cpf
        restoreId
        address {
          id
          zipCode
          street
          number
          complement
          neighborhood
          city
          stateInitials
          stateName
          owner
        }
        owner
      }
      nextToken
    }
  }
`;
export const getAddress = /* GraphQL */ `
  query GetAddress($id: ID!) {
    getAddress(id: $id) {
      id
      zipCode
      street
      number
      complement
      neighborhood
      city
      stateInitials
      stateName
      owner
    }
  }
`;
export const listAddresss = /* GraphQL */ `
  query ListAddresss(
    $filter: ModelAddressFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listAddresss(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        zipCode
        street
        number
        complement
        neighborhood
        city
        stateInitials
        stateName
        owner
      }
      nextToken
    }
  }
`;
export const getCategory = /* GraphQL */ `
  query GetCategory($id: ID!) {
    getCategory(id: $id) {
      id
      name
      exams {
        items {
          id
          categoryId
          title
          subtitle
          description
          price
        }
        nextToken
      }
    }
  }
`;
export const listCategorys = /* GraphQL */ `
  query ListCategorys(
    $id: ID
    $filter: ModelCategoryFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listCategorys(
      id: $id
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        id
        name
        exams {
          nextToken
        }
      }
      nextToken
    }
  }
`;
export const getExam = /* GraphQL */ `
  query GetExam($id: ID!) {
    getExam(id: $id) {
      id
      categoryId
      title
      subtitle
      description
      price
    }
  }
`;
export const listExams = /* GraphQL */ `
  query ListExams(
    $filter: ModelExamFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listExams(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        categoryId
        title
        subtitle
        description
        price
      }
      nextToken
    }
  }
`;
export const getKit = /* GraphQL */ `
  query GetKit($id: ID!) {
    getKit(id: $id) {
      id
      categoryId
      examId
      owner
      status
    }
  }
`;
export const listKits = /* GraphQL */ `
  query ListKits(
    $filter: ModelKitFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listKits(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        categoryId
        examId
        owner
        status
      }
      nextToken
    }
  }
`;
