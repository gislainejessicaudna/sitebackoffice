import propTypes from 'prop-types';
import React from 'react';

import './styles.scss';

const ContainerView = ({
  children,
}) => (
  < div className="Container-default-lay">
    { children }
  </div>
);

ContainerView.propTypes = {
  children: propTypes.shape().isRequired,
};

export default ContainerView;
