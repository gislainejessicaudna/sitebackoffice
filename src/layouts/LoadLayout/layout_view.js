import propTypes from 'prop-types';
import React from 'react';

import {
  Container,
  LoadView,
} from './styles';

const ContainerView = ({
  loading, children,
}) => (
  <Container>
    {loading && (
      <LoadView>
{' '}
<h1> Carregando ... </h1>
{' '}
</LoadView>
    )}
    { children }
  </Container>
);

ContainerView.propTypes = {
  loading: propTypes.bool,
  children: propTypes.shape().isRequired,
};

ContainerView.defaultProps = {
  loading: false,
};

